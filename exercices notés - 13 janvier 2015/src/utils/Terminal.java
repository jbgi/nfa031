package utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Interactions avec le terminal
 */
public final class Terminal {

	private Terminal() {
		throw new IllegalStateException();
	}

	// Lire un String
	public static String lireString() {
		return readLine().run();
	}

	// Lire un entier
	public static int lireInt() {
		return readLine().map(Integer::parseInt).run();
	}

	// Lire un boolean
	public static boolean lireBoolean() {
		return readLine().map(Boolean::parseBoolean).run();
	}

	// Lire un double
	public static double lireDouble() {
		return readLine().map(Double::parseDouble).run();
	}

	// Lire un caractere
	public static char lireChar() {
		return readLine().map(s -> s.isEmpty() ? '\n' : s.charAt(0)).run();
	}

	// Afficher un String
	public static void ecrireString(String s) {
		print(s).run();
	}

	// Afficher un String avec saut de ligne
	public static void ecrireStringln(String s) {
		printNewLine(s).run();
	}

	// Afficher un entier
	public static void ecrireInt(int i) {
		print(Integer.toString(i)).run();
	}

	// Afficher un entier puis saut de ligne
	public static void ecrireIntln(int i) {
		printNewLine(Integer.toString(i)).run();
	}

	// Afficher un boolean
	public static void ecrireBoolean(boolean b) {
		print(Boolean.toString(b)).run();
	}

	// Afficher un boolean puis saut de ligne
	public static void ecrireBooleanln(boolean b) {
		printNewLine(Boolean.toString(b)).run();
	}

	// Afficher un double
	public static void ecrireDouble(double d) {
		print(Double.toString(d)).run();
	}

	// Afficher un double puis saut de ligne
	public static void ecrireDoubleln(double d)
	{
		printNewLine(Double.toString(d)).run();
	}

	 // Afficher un caractere
	public static void ecrireChar(char c)
	{
		print(Character.toString(c)).run();
	}

	// Afficher un caractere puis saut de ligne
	public static void ecrireCharln(char c)
	{
		printNewLine(Character.toString(c)).run();
	}

	// saut de ligne
	public static void sautDeLigne() {
		newLine().run();
	}

	/**
	 * Lecture du terminal
	 *
	 * @return Une operation de lecture d'une ligne tapée dans le terminal
	 */
	public static IO<String> readLine() {
		return () -> new BufferedReader(new InputStreamReader(System.in))
				.readLine();
	}

	/**
	 * Écriture dans le terminal
	 *
	 * @param msg
	 *            un message à afficher dans le terminal
	 * @return Une operation d'écriture du message dans le terminal
	 */
	public static IO<Unit> print(String msg) {
		return () -> {
			System.out.print(msg);
			return Unit.unit();
		};
	}

	/**
	 * Passer à la ligne dans le terminal
	 *
	 * @return une opération de passage à la ligne dans le terminal
	 */
	public static IO<Unit> newLine() {
		return () -> {
			System.out.println();
			return Unit.unit();
		};
	}

	/**
	 * Écriture + à la ligne dans le terminal
	 *
	 * @param msg
	 *            un message à afficher dans le terminal
	 * @return une opération d'écriture du message suivit d'un saut de ligne
	 *         dans le terminal
	 */
	public static IO<Unit> printNewLine(String msg) {
		return print(msg).flatMap(u -> newLine());
	}
}

package utils;

import java.io.IOException;
import java.util.function.Function;

/**
 * Classe utilisé pour représenté les opération d'entrée/sortie (Input/Ouput),
 * tels que les interractions avec le terminal.
 *
 * @param <A>
 *            le type de la valeur produite par l'opération d'entrée/sortie
 *            lorsqu'elle est exécutée (méthode {@link IO#run()})
 */
@FunctionalInterface
public interface IO<A> {

	/**
	 * Exécute l'opération d'entré/sortie.
	 *
	 * @return la valeur produite par l'exécution
	 * @throws IOException
	 *             en cas d'erreur lors de l'exécution
	 */
	A runChecked() throws IOException;

	/**
	 * Idem {@link #runChecked()()} en enveloppant l'exception dans une
	 * exception non-checké.
	 *
	 * @return la valeur produite par l'exécution
	 * @throws RuntimeException
	 *             en cas d'erreur lors de l'exécution
	 */
	default A run() {
		final A result;
		try {
			result = runChecked();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	/**
	 * Créer une nouvelle opération d'entrée sortie avec transformation de la
	 * valeur de retour.
	 *
	 * @param f
	 *            la fonction de transformation de la valeur de retour
	 * @return une nouvelle opération d'entrée sortie avec transformation de la
	 *         valeur de retour
	 */
	default <B> IO<B> map(final Function<A, B> f) {
		return () -> f.apply(runChecked());
	}

	/**
	 * Créer une nouvelle opération d'entrée sortie qui utilise de la valeur de
	 * retour originale pour une opération IO suivante.
	 *
	 * @param f
	 *            la fonction de transformation de la valeur de retour
	 * @return une nouvelle opération d'entrée sortie qui utilise de la valeur
	 *         de retour originale pour une opération IO suivante.
	 */
	default <B> IO<B> flatMap(final Function<A, IO<B>> f) {
		return () -> f.apply(runChecked()).runChecked();
	}

	/**
	 * Créer une opération IO à partir d'une valeur simple : garentie de
	 * retourné cette même valeur lors de l'exécution (méthode {@link #run()})
	 *
	 * @param a
	 *            valeur à transformé en opération IO
	 * @return une opération IO qui produit toujours la valeur a.
	 */
	public static <A> IO<A> pure(final A a) {
		return () -> a;
	}

}
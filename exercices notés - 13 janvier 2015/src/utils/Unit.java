package utils;

/**
 * Type utilisé pour représenter le résulat d'un pur effet de bord : c'est à
 * dire qui ne produit aucune valeur exploitable par le programme (par exemple
 * écrire quelque chose dans le terminal). Analogue au type {@link Void}, mais
 * founrni avec une (unique) instance non-null.
 *
 */
public final class Unit {

	private static final Unit unit = new Unit();

	private Unit() {
		super();
	}

	/**
	 * @return unit
	 */
	public static Unit unit() {
		return unit;
	}

}
package correction;

import utils.Terminal;

public class Exercice4 {
    public static void main(final String[] args) {
        int choix;
        int[][] mat = lireMatriceVotes();
        do {
            Terminal.ecrireStringln("Quelle action ? \n 1 : réinitialiser données\n 2 : calcule nb de personnes\n"
                    + " 3 : calcule intention vote\n 4 : pourcentage vote\n 5 : répartition H/F\n 0 : fin");
            choix = Terminal.lireInt();
            if (choix == 1) {
                mat = lireMatriceVotes();

            } else if (choix == 2) {
                Terminal.ecrireStringln("Nombre de personnes sondées: " + calculeNbPersonnesSondes(mat));

            } else if (choix == 3) {
                int intention;
                do {
                    Terminal.ecrireString("Intention de vote (0=OUI, 1=NON, 2=BLANC): ");
                    intention = Terminal.lireInt();
                } while ((intention < 0) || (intention > 2));
                Terminal.ecrireStringln("Nombre de personnes: " + calculeIntention(mat, intention));

            } else if (choix == 4) {
                final double pcOui = pourcentageOui(mat);
                final double pcNon = 100d - pcOui;
                Terminal.ecrireStringln("Resultat sur suffrage exprime:\n\tOUI\tNON");
                Terminal.ecrireString("\t");
                Terminal.ecrireDouble(pcOui);
                Terminal.ecrireString("\t");
                Terminal.ecrireDouble(pcNon);
                Terminal.sautDeLigne();

            } else if (choix == 5) {
                int intention;
                do {
                    Terminal.ecrireString("Intention de vote (0=OUI, 1=NON, 2=BLANC): ");
                    intention = Terminal.lireInt();
                } while ((intention < 0) || (intention > 2));
                final double pourcentageHF = pourcentageHF(mat, intention);
                Terminal.ecrireStringln("Homme: " + pourcentageHF + ", Femme: " + (100d - pourcentageHF));

            } else if (choix == 0) {
                Terminal.ecrireStringln("Au revoir");
            }
        } while (choix != 0);
    }

    static int[][] lireMatriceVotes() {

        final int[] votesHommes = lireLigneMatriceVotes("d'hommes");
        final int[] votesFemmes = lireLigneMatriceVotes("de femmes");

        return new int[][] { votesHommes, votesFemmes };
    }

    private static int[] lireLigneMatriceVotes(final String categorie) {

        final int[] votes = new int[3];

        final String[] labelVote = new String[] { "OUI", "NON", "BLANC" };

        for (int i = 0; i < labelVote.length; i++) {
            Terminal.ecrireString("Entrer nombre " + categorie + " votant " + labelVote[i] + " : ");

            votes[i] = Terminal.lireInt();
        }
        return votes;
    }

    static long calculeNbSuffrageExprime(final int[][] m) {
        long nbPersonnes = 0;
        for (final int[] ligne : m) {
            nbPersonnes = nbPersonnes + ligne[0] + ligne[1];
        }
        return nbPersonnes;
    }

    static long calculeNbPersonnesSondes(final int[][] m) {
        long nbPersonnes = 0;
        for (final int[] ligne : m) {
            for (final int i : ligne) {
                nbPersonnes = nbPersonnes + i;
            }
        }
        return nbPersonnes;
    }

    static long calculeIntention(final int[][] m, final int intention) {
        long nbPersonnes = 0;
        for (final int[] ligneVotes : m) {
            nbPersonnes = nbPersonnes + ligneVotes[intention];
        }
        return nbPersonnes;
    }

    static double pourcentageOui(final int[][] m) {
        final double total = calculeNbSuffrageExprime(m);
        final double nbOui = calculeIntention(m, 0);
        return (100d * nbOui) / total;
    }

    static double pourcentageHF(final int[][] m, final int intention) {
        final double totalIntention = calculeIntention(m, intention);
        return (m[0][intention] * 100d) / totalIntention;
    }
}
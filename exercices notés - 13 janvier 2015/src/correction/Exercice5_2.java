package correction;

import utils.Terminal;

public class Exercice5_2 {

    public static void main(final String[] args) {
        final int[] clef = { 2, 3, 7, 8, 1 };
        Terminal.ecrireString("Chaine a encoder/decoder: ");
        final String s = Terminal.lireString();
        final String encode = encode(s, clef);
        Terminal.ecrireStringln("encodage: " + encode);
        Terminal.ecrireStringln("decodage: " + decode(encode, clef));
    }

    static boolean estUneLettre(final char c) {
        return ((c >= 'a') && (c <= 'z'));
    }

    static String encode(final String s, final int[] clef) {
        final char[] c = s.toLowerCase().toCharArray();
        // k: pour parcourir le tableau de la clef
        int k = 0;
        for (int i = 0; i < c.length; i++) {
            final char charactere = c[i];
            if (estUneLettre(charactere)) {
                c[i] = decaleVersLaDroite(charactere, clef[k]);
                k = (k + 1) % clef.length;
            }
        }

        return new String(c);
    }

    static char decaleVersLaDroite(final char c, final int nbDecalage) {
        final int indexDepuisLettreA = c - 'a';

        final int indexDepuisLettreAapresDecallage = (indexDepuisLettreA + nbDecalage) % 26;

        return (char) ('a' + indexDepuisLettreAapresDecallage);
    }

    static String decode(final String s, final int[] clef) {
        final char[] c = s.toLowerCase().toCharArray();
        // k: pour parcourir le tableau de la clef
        int k = 0;
        for (int i = 0; i < c.length; i++) {
            final char charactere = c[i];
            if (estUneLettre(charactere)) {
                c[i] = decaleVersLaGauche(charactere, clef[k]);
                k = (k + 1) % clef.length;
            }
        }

        return new String(c);
    }

    static char decaleVersLaGauche(final char c, final int nbDecalage) {
        return decaleVersLaDroite(c, 26 - nbDecalage);

    }

}
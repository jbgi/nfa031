package correction;

import java.util.function.LongBinaryOperator;
import java.util.stream.LongStream;

import utils.Terminal;

public class Exercice1 {

    public static void main(final String[] args) {
        Terminal.ecrireString("Entrer un entier n >= 1: ");
        final int n = Terminal.lireInt();
        Terminal.ecrireStringln("1 + 2² + 3² + ... + (n-1)² + n² = " +
                sommeDesCarresRecusif(n));
    }

    static long sommeDesCarresRecusif(final long n) {
        return (n == 0) ? 0 // cas de base
                : sommeDesCarresRecusif(n - 1) + (n * n); // cas de propagation
    }

    /* Essayons maintenant d'utiliser l'interface Stream pour résoudre le
     * problème : */

    static long sommeDesCarresStream(final long n) {
        return LongStream
                .rangeClosed(1, n)/* Définition de la suite des entier naturel à
                                   * partir de 1 jusqu'à n */

                .map((final long x) -> (x * x)) /* transformation de chaque
                                                 * entier avec la fonction
                                                 * carré: on obtient la suite
                                                 * des carrés jusqu'à n² */
                // .sum()
                .reduce(0, (x, y) -> x + y); /* permet de 'résumer' (réduire) la
                                              * suite des carré optenu à l'étape
                                              * précedente en un seul entier, en
                                              * utilisant 0 comme 'résumé'
                                              * initial et l'addition, c-à-d la
                                              * fonction (long x, long y) -> x +
                                              * y, comme opération pour
                                              * 'résumer' deux valeurs entière
                                              * en une seule. 0 est aussi appelé
                                              * élément identité pour
                                              * l'addition, car x + 0 = 0, et
                                              * est renvoyé si la liste est
                                              * vide, c'est à dire si n < 1) */
    }

    /* La traduction en utlisant une boucle 'for' cela donne : */

    static long sommeDesCarresAvecFor(final long n) {

        long resume = 0;/* <- résumé initial (renvoyé si n < 1) */
        for (int i = 1; i <= n; i++) {/* <- correspond à la définition de la
                                       * suite 'rangeClosed(1, n)' */
            final long y = i * i; /* <- correspond à l'étape 'map' */
            resume = resume + y; /* reduce avec ((x, y) -> x + y) */
        }
        return resume;

    }

    /* On pourrait aussi passer l'élément identité et l'opération de réduction
     * en paramètre à une autre méthode, à la manière de la classe Stream: */

    static long sommeDesCarresFor2(final long n) {

        return sommeDesCarresFor(n, 0, (final long x, final long y) -> x + y);

    }

    static long sommeDesCarresFor(final long n, final long identity, final LongBinaryOperator operator) {

        long resume = identity; // identity: résumé initial (renvoyé si n < 1)
        for (int i = 1; i <= n; i++) {// rangeClosed(1, n)
            final long y = i * i; // map
            resume = operator.applyAsLong(resume, y); // reduce avec operator
        }
        return resume;

    }

}
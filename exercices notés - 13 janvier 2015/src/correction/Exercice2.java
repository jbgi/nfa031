package correction;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiPredicate;

import utils.Terminal;

public class Exercice2 {

    public static void main(final String[] args) {

        final char c = 'w';

        final String s = "Hello World !";

        final List<Character> listChar = Arrays.asList('H', 'e', 'l', 'l', 'o');
        // la liste ci-dessus est équivalente au tableau:
        final char[] charTab = new char[] { 'H', 'e', 'l', 'l', 'o' };

        Terminal.ecrireStringln("Trouvé (vrai) " + c + " : " + contientChar(s.toCharArray(), c));

        Terminal.ecrireStringln("Trouvé (vrai) " + c + " : " + contientChar(listChar, c));

        final char z = 'z';

        Terminal.ecrireStringln("Trouvé (faux) " + z + " : " + contientChar(s.toCharArray(), z));

        Terminal.ecrireStringln("Nombre de fois (3) : " + compteChar(s.toCharArray(), c));

        final String s2 = "Hello! I'm the World.";

        Terminal.ecrireStringln("Trouvé tab (vrai) : " + contientTout(s.toCharArray(), s2.toCharArray()));

        final String s3 = "Hello. World.";

        Terminal.ecrireStringln("Trouvé tab (faux) : " + contientTout(s.toCharArray(), s3.toCharArray()));

        Terminal.ecrireStringln("Equals (faux)  : " + equals(s.toCharArray(), s2.toCharArray()));

        Terminal.ecrireStringln("Equals (vrai)  : " + equals(s.toCharArray(), s.toCharArray()));
    }

    // Q1
    static boolean contientChar(final char[] tab, final char c) {
        boolean contientChar = false;
        for (int i = 0; (i < tab.length) && !contientChar; i++) {
            contientChar = (c == tab[i]);
        }
        return contientChar;
    }

    static boolean contientChar(final List<Character> list, final char c) {
        return list.stream().anyMatch(ch -> ch.charValue() == c);
    }

    // Q2
    static int compteChar(final char[] tab, final char c) {
        int compteur = 0;
        for (final char val : tab) {
            if (val == c) {
                compteur++;
            }
        }
        return compteur;
    }

    static long compteChar(final List<Character> list, final char c) {

        return list.stream().filter(ch -> ch.charValue() == c).count();
    }

    // Q3
    static boolean contientTout(final char[] t1, final char[] t2) {
        boolean contientTout = true;
        for (int i = 0; (i < t1.length) && !contientTout; i++) {
            contientTout = contientChar(t2, t1[i]);
        }
        return contientTout;
    }

    static boolean contientTout(final List<Character> t1, final List<Character> t2) {

        return t1.stream().allMatch(ch -> contientChar(t2, ch));
    }

    // Q4
    static boolean equals(final char[] tab1, final char[] tab2) {
        boolean result;
        if (tab1.length != tab2.length) {
            result = false;
        } else {

            result = true;
            for (int i = 0; (i < tab1.length) && result; i++) {
                result = tab1[i] == tab2[i];
            }
        }
        return result;
    }

    static boolean equalsListChar(final List<Character> tab1, final List<Character> tab2) {
        boolean result;
        if (tab1.size() != tab2.size()) {
            result = false;
        } else {

            result = true;
            for (int i = 0; (i < tab1.size()) && result; i++) {
                result = tab1.get(i).charValue() == tab2.get(i).charValue();
            }
        }
        return result;
    }

    static boolean equalsListChar2(final List<Character> l1, final List<Character> l2) {
        return equalsGeneric(l1, l2, (c1, c2) -> c1.charValue() == c2.charValue());
    }

    static boolean equalsListString(final List<String> l1, final List<String> l2) {
        return equalsGeneric(l1, l2, (s1, s2) -> equals(s1.toCharArray(), s2.toCharArray()));
    }

    static boolean equalsListInteger(final List<Integer> l1, final List<Integer> l2) {
        return equalsGeneric(l1, l2, (i1, i2) -> i1.intValue() == i2.intValue());
    }

    /* Un 'BiPredicate' est une fonction qui prend deux argument et qui retourne
     * un boolean. ici la fonction equalsTest est de type BiPredicate<T, T> :
     * elle prend deux arguments de même type (T, qui peut être nimporte quel
     * type) et doit renvoyer true si les deux arguments ont la même valeur,
     * false sinon */

    static <T> boolean equalsGeneric(final List<T> l1, final List<T> l2, final BiPredicate<T, T> equalsTest) {
        boolean result;
        if (l1.size() != l2.size()) {
            result = false;
        } else {
            result = true;
            for (int i = 0; (i < l1.size()) && result; i++) {
                result = equalsTest.test(l1.get(i), l2.get(i));
            }
        }
        return result;
    }
}
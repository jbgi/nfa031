package correction;

import utils.Terminal;

public class Exercice5_1 {

    public static void main(final String[] args) {
        Terminal.ecrireString("Chaine a encoder/decoder: ");
        final String s = Terminal.lireString();
        final String encode = encode(s);
        Terminal.ecrireStringln("encodage: " + encode);
        Terminal.ecrireStringln("decodage: " + decode(encode));
    }

    static boolean estUneLettre(final char c) {
        return ((c >= 'a') && (c <= 'z'));
    }

    static String encode(final String s) {
        final char[] c = s.toLowerCase().toCharArray();
        for (int i = 0; i < c.length; i++) {
            final char charactere = c[i];
            if (estUneLettre(charactere)) {
                c[i] = decaleVersLaDroite(charactere, 3);
            }
        }

        return new String(c);
    }

    static char decaleVersLaDroite(final char c, final int nbDecalage) {
        final int indexDepuisLettreA = c - 'a';

        final int indexDepuisLettreAapresDecallage = (indexDepuisLettreA + nbDecalage) % 26;

        return (char) ('a' + indexDepuisLettreAapresDecallage);
    }

    static String decode(final String s) {
        final char[] c = s.toLowerCase().toCharArray();
        for (int i = 0; i < c.length; i++) {
            final char charactere = c[i];
            if (estUneLettre(charactere)) {
                c[i] = decaleVersLaGauche(charactere, 3);
            }
        }

        return new String(c);
    }

    static char decaleVersLaGauche(final char c, final int nbDecalage) {
        return decaleVersLaDroite(c, 26 - nbDecalage);

    }
}

package correction;

import utils.Terminal;

public class Exercice3 {

    public static void main(final String[] args) {
        Terminal.ecrireStringln("Les 100 premiers nombres premiers sont: ");
        Terminal.ecrireStringln(java.util.Arrays.toString(nPremiers(100)));
    }

    static int[] nPremiers(final int n) {
        final int[] nPremiers = new int[n];
        int nombre = 1;
        int i = 0; // nombre de nombre premier trouvé
        while (i < n) {
            if (estPremier(nombre)) {
                nPremiers[i] = nombre;
                i = i + 1;
            }
            nombre = nombre + 2; // on teste que les nombre impaires
        }
        return nPremiers;
    }

    static boolean estPremier(final int n) {
        boolean nPremier = true;
        // on vérifie si il existe un nombre i entre 2 et n-1 qui soit un
        // diviseur de n:
        for (int i = 2; (i <= Math.sqrt(n)) && nPremier; i++) {
            nPremier = (n % i) != 0; // on sort de la boucle dès que le test est
            // à false
        }
        // si n est divisible par i, alors n n'est pas premier
        return nPremier;
    }
}
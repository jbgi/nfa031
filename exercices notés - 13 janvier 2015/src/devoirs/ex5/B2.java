package devoirs.ex5;

import utils.Terminal;

public class B2 {
    public static void main(final String[] args) {

        // création de la clef : J'aimerai que cette clef permette de créer
        // automatiquement le tableau "affectationclef", mais je n'y arrive
        // pas...
        final int clef[] = new int[] { 2, 3, 7, 8, 1 };
        /*
         * CORRECTION: pour raison de performance, il vaut mieux de toute façon
         * éviter de dupliquer des donnés en mémoire : il faut trouver une autre
         * astuce...
         */

        final char alphabet[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                'r', 's', 't', 'u', 'v', 'w', 'y', 'z' };

        // création d'un tableau répétant la clef : JE N'ARRIVE PAS A GENERER CE
        // TABLEAU AUTOMATIQUEMENT A PARTIR DE LA CLEF
        final int[] affectationclef = new int[] { 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7,
                8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3,
                7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2,
                3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1, 2, 3, 7, 8, 1 };

        Terminal.ecrireString("Ecrire votre message : ");
        final String Message = Terminal.lireString();

        final char Votremessage[] = Message.toCharArray();
        final char MessageCode[] = new char[10000];

        for (final char element : Votremessage) {
            Terminal.ecrireString(" " + element + " ");
        }
        Terminal.ecrireStringln("");

        Terminal.ecrireString("Votre message codé est : ");

        int x = 0;
        for (int i = 0; i < Votremessage.length; i++) {
            if (estUneLettre(alphabet, Votremessage[i]) == true) {
                /*
                 * CORRECTION: 'p == true' est équivalent à 'p'. de même 'p ==
                 * false' est équivalent à '!p' (préférer la seconde forme)
                 */

                MessageCode[i] = decalerLettre(Votremessage[i], affectationclef[x]);
                x = x + 1;
                /*
                 * CORRECTION: l'astuce: utiliser le modulo (%) pour boucler sur
                 * les éléments de la clef: x = (x+1) % clef.length;
                 */
            } else {
                MessageCode[i] = Votremessage[i];
            }
            Terminal.ecrireString(" " + MessageCode[i] + " ");
        }
        /*
         * CORRECTION: Il manque le déchiffrement...
         */
    }

    static char decalerLettre(final char lettre, final int n) {

        // on calcule l'index de 'lettre' dans l'alphabet, 'a' ayant l'index 0.
        final int indexDepuisLettreA = lettre - 'a';

        final int indexDepuisLettreAapresDecallage = (indexDepuisLettreA + n) % 26;
        // le modulo 26 permet de retrouver l'index dans l'alphabet normal (non
        // cyclique), à partir de 'a'

        return (char) ('a' + indexDepuisLettreAapresDecallage);

    }

    static boolean estUneLettre(final char alphabet[], final char c) {
        boolean appartientlaphabet = false;
        for (final char element : alphabet) {
            if (c == element) {
                appartientlaphabet = true;
                break;
            } else {
                appartientlaphabet = false;
            }
        }
        return appartientlaphabet;
    }

}

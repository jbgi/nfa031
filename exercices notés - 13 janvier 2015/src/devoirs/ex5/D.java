package devoirs.ex5;

import utils.Terminal;

public class D {
    public static void main(final String[] args) {// concernant la première question le code de césar j'avais dans un premier
        // temps déclaré une clé constante de 3 mais en factorisant mon code le plus
        // possible (je souhaitais
        // ne faire qu'une fonction pour le chiffrement / dechiffrement au lieu de 2)
        // la première question à laissé place à la deuxième du coup je vérifiais le fonctionnement en mettant 3 comme clé
        // j'espère que vous n'attendiez pas nécessairement une fonction séparée uniquement
        // pour un décalage à pas fixe.
        // CORRECTION: l'explication est correcte, la question 1 est équivalente à la question 2 avec comme clée {3} ! vous avez
        // correctement appliqué un principe fondamental de la programmation, qui est "DRY" = "Don't Repeat Yourself"!

        Terminal.ecrireStringln("Entrez un mot ou une phrase à chiffrer : ");
        final String chAChiffrer = Terminal.lireString();
        Terminal.ecrireStringln("Entrez la clé numérique de chiffrement : ");
        final String LacleC = Terminal.lireString();
        Terminal.ecrireStringln("Le chiffrement de : " + chAChiffrer + " avec la cle " + LacleC + " donne : "
                + (ParcoursChaineA(chAChiffrer, LacleC, 0)));

        Terminal.ecrireStringln("Entrez un mot ou une phrase à déchiffrer : ");
        final String chADeChiffrer = Terminal.lireString();
        Terminal.ecrireStringln("Entrez la clé numérique de dechiffrement : ");
        final String LacleD = Terminal.lireString();
        Terminal.ecrireStringln("Le Dechiffrement de : " + chADeChiffrer + " avec la cle " + LacleD + " donne : "
                + (ParcoursChaineA(chADeChiffrer, LacleD, 1)));
    }

    // bonjour, comment ça va?
    // 23781
    private static String ParcoursChaineA(final String chAChiffrer, final String cle, final int choix) {// fonction de parcours
        // de la chaine de
        // l'utilisateur en
        // simultané avec la
        // clé.
        String result = "";
        int j = 0;
        for (int i = 0; i < chAChiffrer.length(); i++) {
            if (j == cle.length()) {
                j = 0;
            }
            if ((chAChiffrer.charAt(i) >= 97) && (chAChiffrer.charAt(i) <= 122)) {// on definit bornes inf et sup dans les codes
                // de la table ascii pour ne gérer que les
                // lettres minuscules.
                result += chidefr(chAChiffrer.charAt(i),// appel de la fonction de chiffrement/dechiffrement avec les 3
                        // paramètres requis.
                        Character.getNumericValue(cle.charAt(j)), choix);
                j++;// on incrémente la position dans la clé.
            } else {
                result += chAChiffrer.charAt(i);// on renvoi le char à position i si on est en dehors bornes.
            }
        }
        return result;
    }

    private static char chidefr(final char lettreChiffre, final int cle, final int choix) {
        int i = lettreChiffre;
        switch (choix) {
        case 0:// paramètre pour le chiffrement
            if ((i >= ((122 - cle) + 1)) && (i <= 122)) {// gestion du fait de "boucler" sur les 26 lettres de l'alphabet.
                i -= 26 - cle;// décalage
            } else {
                i += cle;
            }
            break;
        case 1: // paramètre pour le dechiffrement
            if ((i >= 97) && (i <= ((97 + cle) - 1))) {// gestion du fait de "boucler" sur les 26 lettres de l'alphabet.
                i += 26 - cle;// décalage
            } else {
                i -= cle;
            }
            break;
        default: // sortie au cas ou choix != de 0 ou 1 (probablement mauvaise pratique) mais je ne savais pas trop quoi mettre
            // d'autre comme default sachant que le choix est passé en paramètre lors de l'appel dans la main...
            // CORRECTION: la technique serait de passer une fonction (de type BiFunction<Charactere, Integer, Charactere>) en
            // argument de ParcoursChaineA, à la place de choix. Et c'est cette fontion qui dans un cas ferait soit le
            // chiffrement soit le déchiffrement.
            break;
        }

        final char lettreEnClair = (char) i;

        return lettreEnClair;
    }
}

package devoirs.ex5;

import utils.Terminal;

public class B1 {

    public static void main(final String[] args) {

        final char alphabet[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                'r', 's', 't', 'u', 'v', 'w', 'y', 'z', 'a', 'b', 'c', 'd' };

        Terminal.ecrireString("Ecrire votre message : ");

        /*
         * CORRECTION: convention en java : les noms de variables commencent par
         * une minuscule
         */
        final String Message = Terminal.lireString();

        final char Votremessage[] = Message.toCharArray();
        final char MessageCode[] = new char[10000];
        /*
         * CORRECTION: autant utiliser la bonne taille directement: new
         * char[Votremessage.length]
         */

        for (final char element : Votremessage) {
            Terminal.ecrireString(" " + element + " ");
        }
        Terminal.ecrireStringln("");

        Terminal.ecrireString("Votre message codé est : ");
        for (int i = 0; i < Votremessage.length; i++) {
            final char n = Votremessage[i];
            MessageCode[i] = NcodeCesar(n, alphabet);
            Terminal.ecrireString(" " + NcodeCesar(n, alphabet) + " ");
        }
        Terminal.ecrireStringln(" ");
        Terminal.ecrireString("Votre message décodé est : ");
        for (final char n : MessageCode) {
            Terminal.ecrireString(" " + NdeCodeCesar(n, alphabet) + " ");
        }

    }

    /*
     * CORRECTION: convention en java : les noms de méthode commencent par une
     * minuscule
     */
    // Méthode renvoyant le caractère codé si N est dans le tableau Alphabet
    private static char NcodeCesar(final char n, final char alphabet[]) {
        char ncode = n;
        for (int i = 0; i < alphabet.length; i++) {
            if (n == alphabet[i]) {
                ncode = alphabet[i + 3];
                break;
            } else {
                /*
                 * CORRECTION: cette branche 'else' ne sert à rien : n est déjà
                 * la valeur par défaut de ncode
                 */
                ncode = n;
            }
        }
        return ncode;
    }

    // Méthode pour décoder le message
    private static char NdeCodeCesar(final char n, final char alphabet[]) {
        char nDeCode = n;
        for (int i = alphabet.length - 1; i >= 0; i--) {
            /*
             * CORRECTION: 'i >= 3' serait plus sûr...
             */
            if (nDeCode == alphabet[i]) {
                nDeCode = alphabet[i - 3];
                break;
            } else {
                nDeCode = n;
            }
        }
        return nDeCode;

    }

}
package devoirs.ex5;

import utils.Terminal;

public class A {

    public static void main(final String[] args) {
        Terminal.ecrireStringln("Tapez 1 pour coder en Jules César et 2" + " pour coder selon une clé :");
        int choixInitial = Terminal.lireInt();

        switch (choixInitial) {
        case 1:
            code1();
            break;
        case 2:
            code2();
            break;
        default:
            choixInitial = Terminal.lireInt();
        }
    }

    // Code Jules César
    static void code1() {
        Terminal.ecrireStringln("Tapez 1 pour coder une chaîne, tapez 2 pour" + " décoder une chaîne :");
        int choiceUser = Terminal.lireInt();

        switch (choiceUser) {
        case 1:
            final int x = 3;
            Terminal.ecrireStringln("Saisissez une chaîne à coder :");
            final String chaine = MhChaineACoder();
            Terminal.ecrireStringln(coderUneChaine(chaine, x));
            break;

        case 2:
            final int y = 23;
            Terminal.ecrireStringln("Saisissez une chaîne à décoder :");
            final String chaineBis = MhChaineACoder();
            Terminal.ecrireStringln(coderUneChaine(chaineBis, y));
            break;
        default:
            Terminal.ecrireStringln("Tapez 1 pour coder une chaîne, tapez 2 pour" + " décoder une chaîne :");
            choiceUser = Terminal.lireInt();
        }
    }

    // Code avec clef
    static void code2() {
        Terminal.ecrireStringln("Tapez 1 pour coder une chaîne selon une clé, tapez 2 pour" + " décoder une chaîne :");
        int choiceUser = Terminal.lireInt();

        switch (choiceUser) {
        case 1:
            Terminal.ecrireStringln("Saisissez une nouvelle clef");
            final int[] clef = nouvelleCle();
            Terminal.ecrireStringln("Saisissez une chaîne à coder :");
            final String chaine = MhChaineACoder();

            Terminal.ecrireStringln(coderUneChaineAvecClef(chaine, clef));

            break;

        case 2:
            Terminal.ecrireStringln("Saisissez une nouvelle clef");
            final int[] cle = nouvelleCle();
            Terminal.ecrireStringln("Saisissez une chaîne à décoder :");
            final String chaineBis = MhChaineACoder();
            Terminal.ecrireStringln(decoderUneChaineAvecClef(chaineBis, cle));
            break;
        default:
            Terminal.ecrireStringln("Tapez 1 pour coder une chaîne, tapez 2 pour" + " décoder une chaîne :");
            choiceUser = Terminal.lireInt();
        }
    }

    // Saisie texte à coder ou à décoder par utilisateur.
    static String MhChaineACoder() {
        final String chaineACoder = Terminal.lireString();

        return chaineACoder;
    }

    // Coder une chaîne avec code Jules César.
    static String coderUneChaine(final String chaine, final int x) {

        final char[] caractereACoder = chaine.toCharArray();

        final StringBuilder chaineCodee = new StringBuilder();

        /* CORRECTION: d'un point de vue performance il vaut mieux éviter la
         * concaténation de string dans des boucles. Comme caractereACoder est
         * un nouveau tableau auquel seule la méthode à accès, il tout à fait
         * possible de le modifier directement, puis d'utilier new
         * String(caractereACoder) pour convertir en String. L'autre possibilité
         * est d'utiliser StringBuilder comme ceci: */
        for (final char i : caractereACoder) {
            if (estUneLettre(i)) {
                chaineCodee.append(decalerLettre(i, x));
            } else {
                chaineCodee.append(i);
            }
        }

        // En modifiant le tableau (encore plus performant que StringBuilder,
        // mais pas toujours possible):
        for (int i = 0; i < caractereACoder.length; i++) {
            final char c = caractereACoder[i];
            if (estUneLettre(c)) {
                caractereACoder[i] = decalerLettre(c, x);
            }
        }

        final String result = chaineCodee.toString();

        // return new String(caractereACoder);
        return result;
    }

    // Coder une chaîne avec clef.
    static String coderUneChaineAvecClef(final String chaine, final int cleTableau[]) {

        final char[] caractereACoder = chaine.toCharArray();

        String chaineCodee = "";

        int y = 0;
        for (final char i : caractereACoder) {

            if (i == ' ') {
                chaineCodee += " ";
            } else if (estUneLettre(i)) {
                // On vérifie si y devient égal à la longueur du tableau de clef
                // et auquel cas, on le remet à 0
                if (y == cleTableau.length) {
                    /* CORRECTION: comme y est incrémenté dans tout les cas, il
                     * se peut que y soit > cleTableau.length: on arriverait
                     * alors à une erreur lors de 'cleTableau[y]' */
                    y = 0;
                }
                // ....
                chaineCodee += decalerLettre(i, cleTableau[y]);
                y++;
            } else {
                chaineCodee += i;
            }
            // System.out.println(cleTableau[y]);
            /* CORRECTION: pour que l'algo soit conforme, 'y' ne devrait ètre
             * incrémenté que dans la branche gardé par estUneLettre(i). */

        }

        return chaineCodee;
    }

    // Décoder une chaîne.
    static void decoderUneChaine(final int x) {
        Terminal.ecrireStringln("Saisissez une chaîne à décoder :");
        final String chaineADecoder = Terminal.lireString();

        final char[] caractereADecoder = chaineADecoder.toCharArray();

        String chaineDecodee = "";

        for (final char i : caractereADecoder) {
            if (i == ' ') {
                chaineDecodee += " ";
            } else if (estUneLettre(i)) {
                chaineDecodee += decalerLettre(i, x);
            } else {
                chaineDecodee += i;
            }
        }
        Terminal.ecrireStringln(chaineDecodee);
    }

    // Décoder une chaîne avec clef.
    static String decoderUneChaineAvecClef(final String chaine, final int cleTableau[]) {

        final char[] caractereACoder = chaine.toCharArray();

        String chaineCodee = "";

        int y = 0;
        for (final char i : caractereACoder) {

            if (i == ' ') {
                chaineCodee += " ";
            } else if (estUneLettre(i)) {
                // On vérifie si y devient égal à la longueur du tableau de clef
                // et auquel cas, on le remet à 0
                if (y == cleTableau.length) {
                    y = 0;
                }
                // ....
                chaineCodee += decalerLettre(i, (26 - cleTableau[y]));
            } else {
                chaineCodee += i;
            }
            // System.out.println(cleTableau[y]);
            y++;
        }

        return chaineCodee;
    }

    // Clef entrée par utilisateur.
    static int[] nouvelleCle() {
        final String cle = Terminal.lireString();

        // Transforme le string en tableau de strings
        final char[] cleTableauBis = cle.toCharArray();

        // Transforme le tableau de strings en tableau de int
        final int[] clef = new int[cleTableauBis.length];
        for (int i = 0; i < cleTableauBis.length; i++) {
            clef[i] = Integer.parseInt(String.valueOf(cleTableauBis[i]));
        }
        return clef;
    }

    // Méthode pour décaler une lettre.
    static char decalerLettre(final char lettre, final int n) {
        // on calcule l'index de 'lettre' dans l'alphabet, 'a' ayant l'index 0.
        final int indexDepuisLettreA = lettre - 'a';

        final int indexDepuisLettreAapresDecallage = (indexDepuisLettreA + n) % 26;
        // le modulo 26 permet de retrouver l'index dans l'alphabet normal (non
        // cyclique), à partir de 'a'

        return (char) ('a' + indexDepuisLettreAapresDecallage);
    }

    // Méthode pour vérifier lettre.
    static boolean estUneLettre(final char c) {
        return ((c >= 'a') && (c <= 'z'));
    }
}
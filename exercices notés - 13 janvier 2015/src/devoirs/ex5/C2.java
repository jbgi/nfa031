package devoirs.ex5;

import utils.Terminal;

public class C2 {

    static String CleEncode(final String s, final int[] tabcle) {
        String dru = "";
        for (int i = 0; i < s.length(); i++) {

            dru = dru + CleEncode(s.charAt(i), tabcle[i % tabcle.length]);
        }
        return dru;
    }

    static String CleDecode(final String s,
            final int[] cle) {
        String abc = "";
        for (int i = 0; i < s.length(); i++) {
            abc = abc + CleDecode(s.charAt(i), cle[i % cle.length]);
        }
        return abc;
    }

    static char CleDecode(final char c, final int n) {
        /* CORRECTION: un seul return!!! Un commentaire explicatif pour chaque
         * if aurait été bienvenu! */

        if ((c >= 'A') && (c < ('A' + n))) {
            return (char) ((c + 26) - n);
        }
        if ((c >= 'a') && (c < ('a' + n))) {
            return (char) ((c + 26) - n);
        }
        if ((c >= ('A' + n)) && (c <= 'z')) {
            return (char) (c - n);
        }
        if ((c >= ('a' + n)) && (c <= 'Z')) {
            return (char) (c - n);
        }
        return c;
    }

    static char CleEncode(final char c, final int n) {
        if ((c >= 'A') && (c <= ('Z' - n))) {
            return (char) (c + n);
        }
        if ((c >= 'a') && (c <= ('z' - n))) {
            return (char) (c + n);
        }
        if ((c > ('z' - n)) && (c <= 'z')) {
            return (char) ((c - 26) + n);
        }
        if ((c > ('Z' - n)) && (c <= 'Z')) {
            return (char) ((c - 26) + n);
        }
        return c;
    }

    public static void main(final String[] args) {

        final String st = "bonjourcommentva";
        final int[] cle = { 2, 3, 7, 8, 1 };

        Terminal.ecrireStringln(st + " : " + CleEncode(st, cle));

    }

}
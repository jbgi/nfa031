package devoirs.ex5;

import utils.Terminal;

public class C1 {

    public static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    public static void main(final String[] args) {

        final String LaCle = "DEFGHIJKLMNOPQRSTUVWXYZABC";
        Terminal.ecrireStringln("Entrer le texte à encoder:");

        final String text = Terminal.lireString();

        final String Encodage = Encodage(text, LaCle);
        System.out.println(Encodage);

        Terminal.ecrireStringln("Entrer le texte à décoder:");

        final String textAdecoder = Terminal.lireString();

        // CORRECTION: pour des raison de lisibilité, en java les méthodes et
        // les variables (hors constantes) commencent par une minuscule. Les
        // noms de classes commencent par une majuscule.
        final String Decodage = Decodage(textAdecoder, LaCle);
        System.out.println(Decodage);
    }

    public static String Encodage(final String text, final String LaCle) {
        String LeCode = text.toLowerCase();

        for (int i = 0; i < 26; i++) {
            final char LaCleChar = LaCle.charAt(i);
            final char LAlphabetChar = ALPHABET.charAt(i);
            LeCode = LeCode.replace(LAlphabetChar, LaCleChar);
            // CORRECTION: Cela marche car le message chiffré utilise l'alphabet
            // majuscule et le message d'origine est en minuscule. Sinon le même
            // alphabet était utilisé on riquerait de chiffrer plusieur fois le
            // même caractère.
            // Astuce intéressante... mais du coup on perd la possibilité
            // d'ajouter la distinction des majuscules et minuscules des les
            // messages!

        }
        return LeCode;
    }

    public static String Decodage(final String text, final String LaCle)
    {
        String LeCode = text.toLowerCase();

        for (int i = 0; i < 26; i++)
        {
            final char LaCleChar = LaCle.charAt(i);
            final char LAlphabetChar = ALPHABET.charAt(i);

            LeCode = LeCode.replace(LAlphabetChar, LaCleChar);

        }
        return LeCode;
    }

}

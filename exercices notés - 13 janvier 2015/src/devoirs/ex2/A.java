package devoirs.ex2;

import utils.Terminal;

public class A {

    public static void main(final String args) {
        final char J = 0, a = 0, v = 0;
        final char[] tab1 = new char[] { J, a, v, a };
        ChercheCaracter(tab1);
    }

    public static void ChercheCaracter(final char[] tab1) {

        Terminal.ecrireString("Entrer un caractère :");
        final char n = Terminal.lireChar();
        final boolean AppartientAuTableau = tableauContient(tab1, n);
        Terminal.ecrireString(n + " appartient au tableau : "
                + AppartientAuTableau);
    }

    private static boolean tableauContient(final char[] tab1, final char n) {

        boolean result = false;

        for (final char element : tab1) {
            if (element == n) {
                result = true;
                /* CORRECTION: on peut rajouter une instruction 'break' pour
                 * sortir de la boucle dès que l'on a trouver le caractère. */
            }
        }
        return result;
    }

}

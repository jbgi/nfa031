package devoirs.ex1;

import utils.Terminal;

public class A {

    public static void main(final String[] arguments) {
        Terminal.ecrireString("Entrez un entier naturel : ");
        final int n = Terminal.lireInt();

        Terminal.ecrireString("La somme des carrés de "
                + n + " est : ");
        Terminal.ecrireInt(Sommedescarres(n));
    }

    public static int Sommedescarres(final int n) {
        // CORRECTION: un seul return svp!
        if (n != 0) {
            return (n * n) + Sommedescarres(n - 1);
        } else {
            return 0;
        }
    }
}

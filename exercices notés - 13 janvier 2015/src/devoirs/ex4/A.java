package devoirs.ex4;

import utils.Terminal;

/*
 *
 */
public class A {

    public static void main(final String[] args) {
        final int[][] tableau = tableauDesVotes();
        Terminal.ecrireStringln("Les hommes qui ont voté oui au sondage sont : " + tableau[0][0] + ", non sont : "
                + tableau[0][1] + " et qui ont voté blanc sont : " + tableau[0][2]);
        Terminal.ecrireStringln("Les femmes qui ont voté oui au sondage sont : " + tableau[1][0] + ", non sont : "
                + tableau[1][1] + " et qui ont voté blanc sont : " + tableau[1][2]);
        choiceUser(tableau);
    }

    // choix 1
    // Chaque champ du tableau est modifié quand l'utilisateur entre les
    // nouvelles données.
    static int[][] choix1() {/*
     * CORRECTION: en java on préfère déclarer
     * "int[][] nouveauTab" plustôt que "int
     * nouveauTab[][]" afin que le type soit lu en
     * entier d'un coup.
     */
        // Chaque champ du tableau est modifié quand l'utilisateur entre les
        // nouvelles données.
        /*
         * CORRECTION: il vaut mieux retourner un nouveau tableau plutôt que de
         * modifier le paramètre. Et pour les méthode qui ne font que modifier
         * les arguments il vaut mieux utiliser une méthode void afin qu'il n'y
         * ait pas d'ambiguités
         */
        final int[][] nouveauTab = new int[2][3];
        Terminal.ecrireStringln("Entrez le nombre de votants oui masculins");
        nouveauTab[0][0] = Terminal.lireInt();
        Terminal.ecrireStringln("Entrez le nombre de votants oui féminins");
        nouveauTab[1][0] = Terminal.lireInt();
        Terminal.ecrireStringln("Entrez le nombre de votants non masculins");
        nouveauTab[0][1] = Terminal.lireInt();
        Terminal.ecrireStringln("Entrez le nombre de votants non féminins");
        nouveauTab[1][1] = Terminal.lireInt();
        Terminal.ecrireStringln("Entrez le nombre de votants blanc masculins");
        nouveauTab[0][2] = Terminal.lireInt();
        Terminal.ecrireStringln("Entrez le nombre de votants blanc féminins");
        nouveauTab[1][2] = Terminal.lireInt();
        return nouveauTab;
    }

    // Choix 2
    static void choix2(final int tab[][]) {
        final int calculSomme = calculDeLaSommeDuTableau(tab);
        Terminal.ecrireStringln("Le nombre de personnes ayant voté est de : " + calculSomme);

    }

    // Choix 3
    static void choix3(final int tab[][]) {
        Terminal.ecrireStringln("Choissisez 0 si vous voulez connaître le nombre de votants oui,"
                + " 1, le nombre de votants non et 2 le nombre de votants blanc");
        final int i = Terminal.lireInt();
        final int nbVotantUneIntention = votantPourUneIntention(i, tab);
        Terminal.ecrireStringln("Le nombre de votants " + i + " est de : " + nbVotantUneIntention);
    }

    // Choix4
    static void choix4(final int tab[][]) {
        final double pourcentageOui = lePourcentageDeOui(tab);
        Terminal.ecrireStringln("Le pourcentage de oui lors de ce sondage est de : " + pourcentageOui);
    }

    // Choix5
    static void choix5(final int tab[][]) {
        Terminal.ecrireStringln("Choissisez 0 si vous voulez connaître le nombre de votants oui,"
                + " chez les femmes et chez les hommes ou 1 pour connaître le nombre de votants non");
        final int i = Terminal.lireInt();
        final int nombreVoixHomme = nombreDeVoixHomme(tab, i);
        final int nombreVoixFemme = nombreDeVoixFemme(tab, i);
        Terminal.ecrireStringln("Le nombre d'hommes ayant voté " + i + " est de : " + nombreVoixHomme);
        Terminal.ecrireStringln("Le nombre de femmes ayant voté " + i + " est de : " + nombreVoixFemme);
    }

    // Méthodes
    private static int[][] tableauDesVotes() {
        final int tableauVote[][] = { { 242, 227, 31 }, { 219, 268, 13 } };

        return tableauVote;
    }

    private static void choiceUser(int[][] tableau) {
        Terminal.ecrireStringln("Saisissez un chiffre entre 0 et 5");
        final int choice = Terminal.lireInt();

        // if (choice == 1) {
        // tableau = choix1(tableau);
        // choiceUser(tableau);
        // }
        // else if (choice == 2) {
        // choix2(tableau);
        // choiceUser(tableau);
        // }
        // // ...
        // else {
        // Terminal.ecrireStringln("Le chiffre que vous avez saisi n'est pas compris "
        // + "entre 0 et 5. Saisissez un chiffre entre 0 et 5");
        // choiceUser(tableau);
        // }

        switch (choice) {
        case 1:
            tableau = choix1();
            choiceUser(tableau);
            break;
        case 2:
            choix2(tableau);
            choiceUser(tableau);
            break;
        case 3:
            choix3(tableau);
            choiceUser(tableau);
            break;
        case 4:
            choix4(tableau);
            choiceUser(tableau);
            break;
        case 5:
            choix5(tableau);
            choiceUser(tableau);
            break;
        case 0:
            Terminal.ecrireStringln("Fin du programme.");
            break;
        default:
            Terminal.ecrireStringln("Le chiffre que vous avez saisi n'est pas compris "
                    + "entre 0 et 5. Saisissez un chiffre entre 0 et 5");
            choiceUser(tableau);
        }

    }

    private static int calculDeLaSommeDuTableau(final int tab[][]) {
        int somme = 0;
        for (final int[] element : tab) {
            for (final int element2 : element) {
                somme += element2;
            }
        }
        return somme;
    }

    private static int votantPourUneIntention(final int e, final int tab[][]) {
        return nombreDeVoixFemme(tab, e) + nombreDeVoixHomme(tab, e);
    }

    private static double lePourcentageDeOui(final int tab[][]) {
        final int sommeVotantsOui = votantPourUneIntention(0, tab);
        int sommeTotale = 0;
        for (final int[] element : tab) {
            for (int j = 0; j < (element.length - 1); j++) {
                sommeTotale += element[j];
            }
        }
        final double result = (sommeVotantsOui * 100) / sommeTotale;
        /*
         * CORRECTION: Attention la conversion vers le type double est faite
         * après la division , donc perte des décimales. Mieux vaut utiliser le
         * type double directement pour sommeVotantsOui ou utiliser 100.0.
         */
        return result;
    }

    private static int nombreDeVoixHomme(final int tab[][], final int e) {
        final int result = tab[0][e];
        return result;
    }

    private static int nombreDeVoixFemme(final int tab[][], final int e) {
        final int result = tab[1][e];
        return result;
    }
}
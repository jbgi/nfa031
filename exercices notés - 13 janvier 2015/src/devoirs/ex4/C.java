package devoirs.ex4;

import utils.Terminal;

public class C {

    public static void main(final String[] args) {
        final int[][] sondage = { { 242, 227, 31 }, { 219, 268, 13 } };// initialisation "brute" de la matrice de l'exercice qui
                                                                       // sera
        // utilis� pour les diff�rentes questions.
        int choix = 0;
        Terminal.ecrireStringln("Entrez un num�ro d'instruction (1, 2, 3, 4, 5, 6) :");
        final String str = Terminal.lireString();// on enregistre le choix utilisateur en String pour �viter l'erreur de class
                                                 // terminal(lireInt) au cas ou autre chose qu'un entier est saisi.

        /* CORRECTION: id�allement on pr�fera faire le travaille de parsing un seule fois, en cr�ant une m�thode qui return un
         * Optional<Integer> (cf. JeudDeNim) */
        if (estChoixValide(str)) {
            choix = Integer.parseInt(str);// une fois la nature de la chaine v�rifi�e on la "parse" pour r�cup�rer une valeur
                                          // sous forme d'int.
        } else {
            System.exit(0); // Le programme se ferme si choix non valide.
            /* CORRECTION: en r�gle g�n�rale il vaut mieux �viter System.exit (car minimise la r�utilisation du programme), et
             * si on l'utilise il faut un argument autre que 0, car 0 indique qu'il n'y pas eu d'erreur */
        }
        // CORRECTION: "while (estChoixValide(choix))" suffit
        while (estChoixValide(choix) == true) {// choix et appel de questions diff�rentes en fonctions du choix utilisateur.
            switch (choix) {
            case 1:
                question1(sondage);
                break;
            case 2:
                question2(sondage);
                break;
            case 3:
                question3(sondage);
                break;
            case 4:
                question4(sondage);
                break;
            case 5:
                question5(sondage);
                break;
            case 6:
                question6();
                break;
            default:
                question6();
                break;
            }
        }

    }

    private static boolean estChoixValide(final String choix) {// gestion d'erreur du choix renvoi une erreur format si valeur
                                                               // non numerique.
        try {
            Integer.parseInt(choix);
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    private static boolean estChoixValide(final int choix) {// j'ai dout� la dessus je voulais essayer de surcharger une m�thode
                                                            // �a semble fonctionner pour tester si bornes inf/sup de choix sont
                                                            // bien respect�es. <- effectivement, si les types de la signature
                                                            // de la m�thode sont diff�rents, on peut d�clarer plusieurs m�thode
                                                            // avec le m�me nom.
        boolean estChoixCorrect = false;
        if ((choix >= 1) && (choix <= 6)) {
            estChoixCorrect = true;
        } else {
            estChoixCorrect = false;
        }
        return estChoixCorrect;

        // CORRECTION: on peut simplifier et juste faire: return (choix >= 1) && (choix <= 6);
    }

    static void question1(final int[][] sondage) {// affichage d'une nouvelle matrice r�-initialis�e.
        // final int[][] nouvelleMatrice = ReInitSondage(sondage); // Ceci est un test je voulais v�rifier au cas ou on veuille
        // garder la matrice d'origine telle quelle et travailler sur une copie.
        /* CORRECTION: La m�thode ReInitSondage ne cr�er pas une nouvelle matrice mais modifie la matrice en place (donc
         * 'nouvelleMatrice' pointerai vers le m�me tableau en m�moire que 'sondage'). Dans le doute il est g�n�rale plus s�r de
         * cr�er un nouvel objet. */
        ReInitSondage(sondage);
        Terminal.ecrireStringln("La matrice du nouveau sondage est : ");
        for (final int[] element : sondage) {
            for (int j = 0; j < sondage[0].length; j++) {
                System.out.print(element[j] + " ");
            }
            System.out.println("");
        }
        // CORRECTION: Pourquoi faire un System.exit ici ? -> le programme s'arr�te!
        System.exit(0);
    }

    /* CORRECTION; il est risquer de renvoyer l'argument sondage, car pourrrait croire qu'une nouvelle matrice est cr��e alors
     * que l'argument est modifi�. mieux vaut alors utiliser void (ne rien renvoyer). Ou alors mieux, cr�er vraiment une
     * nouvelle matrice, et alors pas besoin de prendre d'argument */
    private static int[][] ReInitSondage(final int[][] sondage) {// fonction de r�-initialisation de la matrice.
        Terminal.ecrireStringln("Entrez le nombre d'hommes qui votent oui : ");
        final int ho = Terminal.lireInt();
        sondage[0][0] = ho;
        Terminal.ecrireStringln("Entrez le nombre d'hommes qui votent non : ");
        final int hn = Terminal.lireInt();
        sondage[0][1] = hn;
        Terminal.ecrireStringln("Entrez le nombre d'hommes qui votent blanc : ");
        final int hb = Terminal.lireInt();
        sondage[0][2] = hb;
        Terminal.ecrireStringln("Entrez le nombre de femmes qui votent Oui : ");
        final int fo = Terminal.lireInt();
        sondage[1][0] = fo;
        Terminal.ecrireStringln("Entrez le nombre de femmes qui votent non : ");
        final int fn = Terminal.lireInt();
        sondage[1][1] = fn;
        Terminal.ecrireStringln("Entrez le nombre de femmes qui votent blanc : ");
        final int fb = Terminal.lireInt();
        sondage[1][2] = fb;
        return sondage;
    }

    static void question2(final int[][] sondage) {// affichage de la somme des votants.
        final int nbTotalDeVotant = calculNbTotalDeVotant(sondage);
        Terminal.ecrireStringln("Il y a en tout : " + nbTotalDeVotant
                + " votants");
        System.exit(0);
    }

    static int calculNbTotalDeVotant(final int[][] sondage) {// fonction de calcul de la somme des valeurs du sondage.
        int resultat = 0;
        for (final int[] element : sondage) {
            for (int j = 0; j < sondage[0].length; j++) { /* CORRECTION: on pourrait ici aussi utiliser une boucle de type
                                                           * 'foreach' comme au dessus */
                resultat += (element[j]);
            }
            System.out.println(""); // CORRECTION: ne pas faire d'entr�e/sortie si on veut faire une m�thode pure
        }

        return resultat;
    }

    static void question3(final int[][] sondage) {// affichage du total des votes pour une intention donn�.
        Terminal.ecrireStringln("Entrez une intention de vote, 0 pour oui, 1 pour non, 2 pour blanc :");
        final int indexIntention = Terminal.lireInt();
        if (estIntentionValide(indexIntention)) {
            final int intentionDeVote = calculTotalIntention(sondage,
                    indexIntention);
            Terminal.ecrireStringln(" Il y a " + intentionDeVote
                    + " personnes qui ont l'intention de vote "
                    + indexIntention);
        } else {
            Terminal.ecrireStringln(" Intention de vote invalide");
        }
        System.exit(0);
    }

    private static boolean estIntentionValide(final int indexIntention) {// booleen de verification si l'intention est valide.
        return (indexIntention == 0) || (indexIntention == 1)
                || (indexIntention == 2);
    }

    static int calculTotalIntention(final int[][] sondage, final int indexIntention) {// calcul du nombre total d'intention en
                                                                                      // fonction d'une intention donn�e(oui,
                                                                                      // non, blanc).
        int resultat = 0;
        if (indexIntention == 0) {
            resultat = sondage[0][0] + sondage[1][0];
        } else if (indexIntention == 1) {
            resultat = sondage[0][1] + sondage[1][1];
        } else if (indexIntention == 2) {
            resultat = sondage[0][2] + sondage[1][2];
        }
        return resultat;

        // CORRECTION: on peut simplifier et juste faire: return sondage[0][indexIntention] + sondage[1][indexIntention]
    }

    static void question4(final int[][] sondage) {
        final double intentionDeOui = CalculIntentionDeOui(sondage);// on r�cup�re les intentions de oui.
        Terminal.ecrireString(" Il y a " + intentionDeOui
                + " % de personnes qui ont l'intention de voter oui, ");
        Terminal.ecrireStringln("reparties en " + CalculRepartitionH(sondage, 0)// on affiche la repartition hommes/femmes
                                                                                // parmis les oui.
                + " % d'hommes et " + CalculRepartitionF(sondage, 0)
                + " % de femmes.");
        System.exit(0);
    }

    static double CalculIntentionDeOui(final int[][] sondage) {// calcul du % de oui.
        double resultatEnPourcentOui = 0;
        resultatEnPourcentOui = ((sondage[0][0] + sondage[1][0]) * 100.0)
                / CalculEnsIntVoteNonNulles(sondage);
        return resultatEnPourcentOui;
    }

    static int CalculEnsIntVoteNonNulles(final int[][] sondage) {// fonction pour calculer le nombres de "voix exprim�es" non
                                                                 // nulles.
        int resultatEnsIntVoteNonNulles = 0; // CORRECTION : affectation � 0 inutile.
        resultatEnsIntVoteNonNulles = (sondage[0][0] + sondage[1][0]
                + sondage[0][1] + sondage[1][1]);
        return resultatEnsIntVoteNonNulles;
    }

    static void question5(final int[][] sondage) {
        Terminal.ecrireStringln("Quelle intention de vote voulez vous afficher ? 0 pour oui, 1 pour non, 2 pour blanc :");
        final int indexIntention = Terminal.lireInt();
        if (estIntentionValide(indexIntention)) {// appel de fonction de test si l'index est valide.
            final double repartitionVoteH = CalculRepartitionH(sondage,
                    indexIntention);
            final double repartitionVoteF = CalculRepartitionF(sondage,
                    indexIntention);
            Terminal.ecrireStringln(" Il y a " + repartitionVoteH
                    + " % d'hommes et " + repartitionVoteF
                    + " % de femmes qui ont l'intention de vote "
                    + indexIntention);
        } else {
            Terminal.ecrireStringln(" Intention de vote invalide");
        }
        System.exit(0);
    }

    static double CalculRepartitionH(final int[][] sondage, final int indexIntention) {// calcul des pourcentages pour une
                                                                                       // intention donn�e parmis les hommes.
        double resultatEnPourcentH = 0;
        if (indexIntention == 0) {// intention oui
            resultatEnPourcentH = (sondage[0][0] * 100.0)
                    / (sondage[0][0] + sondage[1][0]);
        } else if (indexIntention == 1) {// intention non
            resultatEnPourcentH = (sondage[0][1] * 100.0)
                    / (sondage[0][1] + sondage[1][1]);
        } else if (indexIntention == 2) {// intention blanc
            resultatEnPourcentH = (sondage[0][2] * 100.0)
                    / (sondage[0][2] + sondage[1][2]);
        }
        return resultatEnPourcentH;

        // CORRECTION: return (sondage[0][indexIntention] * 100.0) / (sondage[0][indexIntention] + sondage[1][indexIntention])
    }

    static double CalculRepartitionF(final int[][] sondage, final int indexIntention) {// calcul des pourcentages pour une
                                                                                       // intention donn�e parmis les femmes.
        double resultatEnPourcentF = 0;
        if (indexIntention == 0) {// intention oui
            resultatEnPourcentF = (sondage[1][0] * 100.0)
                    / (sondage[0][0] + sondage[1][0]);
        } else if (indexIntention == 1) {// intention non
            resultatEnPourcentF = (sondage[1][1] * 100.0)
                    / (sondage[0][1] + sondage[1][1]);
        } else if (indexIntention == 2) {// intention blanc
            resultatEnPourcentF = (sondage[1][2] * 100.0)
                    / (sondage[0][2] + sondage[1][2]);
        }
        return resultatEnPourcentF;

        // CORRECTION: pour factoriser:
        // return calculRepartitionSexe(sondage, indexIntention, indexSexe);

    }

    // CORRECTION pour factoriser les calculs:
    static double calculRepartitionSexe(final int[][] sondage, final int indexIntention, final int indexSexe) {// calcul des
                                                                                                               // pourcentages
                                                                                                               // pour une
        // intention donn�e pour un sexe donn�.
        return (sondage[indexSexe][indexIntention] * 100.0) / (sondage[0][indexIntention] + sondage[1][indexIntention]);
    }

    static void question6() {// sortie du programme
        Terminal.ecrireStringln(" AU REVOIR. ");
        System.exit(0);

    }

}
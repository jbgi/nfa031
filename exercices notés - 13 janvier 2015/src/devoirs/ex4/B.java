package devoirs.ex4;

import utils.Terminal;

public class B {

    public static void main(final String[] args) {

        int[][] matrice = new int[2][3];
        int code;
        do {
            Menu();
            code = Terminal.lireInt();
            switch (code) {
            case 0:
                Terminal.ecrireString("\nMaintenant vous avez quitté le programme......");
                break;
            case 1:
                matrice = initMatrice();
                break;
            case 2:
                afficheMatrice(matrice);
                totalVotant(matrice);
                System.out.println();
                break;
            case 3:
                Terminal.ecrireString("Entrer une intention \nindice 0 = Oui \nindice 1 = Non\nindice 2 = Blanc : ");
                final int intention = Terminal.lireInt();
                totalIntention(matrice, intention);
                System.out.println();
                break;
            case 4:
                resultatSondage(matrice);
                break;
            case 5:
                Terminal.ecrireString("Entrer une intention \nindice 0 = Oui \nindice 1 = Non\nindice 2 = Blanc : \n");
                final int intentionIndex = Terminal.lireInt();
                repartition(matrice, intentionIndex);
                break;

            default:
                Terminal.ecrireString("\nVotre choix doit être compris entre 1 et 5 : \n");

            }

        } while (code != 0);

    }

    public static void Menu() {
        Terminal.ecrireStringln("************ MENU ************");
        Terminal.ecrireStringln("\n-1 Initialiser la matrice." + "\n-2 Calculer et afficher le nombre de sondés."
                + "\n-3 Calcul Total d'une intention de vote donné." + "\n-4 Resultat du sondage en pourcentage."
                + "\n-5 Répartition des intention." + "\n-0 Quitter le programme.\n");
        Terminal.ecrireString("Faire un choix : ");

    }

    // initialisation matrice
    public static int[][] initMatrice() {

        Terminal.ecrireString("Entrer nombres d'hommes votant \'oui\' : ");
        final int nbrOuiH = Terminal.lireInt();
        Terminal.ecrireString("entrer nombre d'hommes votants \'NON\' : ");
        final int nbrNonH = Terminal.lireInt();
        Terminal.ecrireString("entrer nombre d'hommes votants  \'blanc\' : ");
        final int nbrBlancH = Terminal.lireInt();

        Terminal.ecrireString("Entrer nombres de femmes votant \'oui\' : ");
        final int nbrOuiF = Terminal.lireInt();
        Terminal.ecrireString("entrer nombre de femmes votant \'NON\' : ");
        final int nbrNonF = Terminal.lireInt();
        Terminal.ecrireString("entrer nombre de femmes votant \'blanc\' : ");
        final int nbrBlancF = Terminal.lireInt();

        final int[][] matrice = new int[2][3];
        // stock le nombre de votant dans la ligne 'Homme'
        matrice[0][0] = nbrOuiH;
        matrice[0][1] = nbrNonH;
        matrice[0][2] = nbrBlancH;

        // stocker le nombre de femme ayant voté dans la ligne 'Femme'
        matrice[1][0] = nbrOuiF;
        matrice[1][1] = nbrNonF;
        matrice[1][2] = nbrBlancF;

        Terminal.sautDeLigne();
        afficheMatrice(matrice);

        return matrice;

    }

    // Affichage matrice
    static void afficheMatrice(final int[][] matrice) {
        for (final int[] element : matrice) {
            for (final int element2 : element) {

                // Terminal.ecrireString(matrice[i][j] + " ");

            }
            Terminal.ecrireStringln("");

        }
    }

    // Affichage du nombre total de personnes prises en compte dans l'enquête
    static int totalVotant(final int[][] matrice) {
        int somme = 0;

        for (final int[] element : matrice) {

            for (final int element2 : element) {
                somme = somme + (element2);
                Terminal.ecrireString(element2 + " ");
            }
            Terminal.ecrireStringln("");
        }
        /*
         * CORRECTION : d'une manière générale il vaut mieux ne pas faire
         * d'entré/sortie (manipulation du terminal) dans les même méthodes que
         * les calculs pures. La raison est que cela limite la réutilisation de
         * votre méthode, qui devient spécifique à un cas d'utilisation très
         * précis.
         */
        Terminal.ecrireString("\nla somme totale des votants : " + somme + "\n");
        return somme;
    }

    // Le pourcentage des oui par rapport au nombre des participant sans les
    // votes blancs
    static void resultatSondage(final int[][] resultat) {
        System.out.println("Le taux des 'Oui' est de : "
                + ((sommeSondage(resultat, 1) * 100.0) / sommeSondage(resultat, 2)) + "%\n\n");
    }

    // methode sera a calculer le pourcentage
    static int sommeSondage(final int[][] resultat, final int index) {
        int somme = 0;

        for (final int[] element : resultat) {
            for (int j = 0; j < index; j++) {
                somme = somme + (element[j]);
            }
        }
        return somme;
    }

    // donne la répartition des intentions de votes entre hommes et femmes.
    static void repartition(final int[][] resultat, final int intentionIndex) {
        int somme = 0;

        for (final int[] element : resultat) {
            somme = somme + (element[intentionIndex]);

        }
        Terminal.ecrireString("le Taux d'hommes votant est : " + ((resultat[0][intentionIndex] * 100) / somme) + "%\n");
        Terminal.ecrireString("le Taux de femmes votant est " + ((resultat[1][intentionIndex] * 100) / somme) + "%\n\n");

    }

    // Somme d'une intention de vote donnée
    static void totalIntention(final int[][] resultat, final int index) {
        int somme = 0;

        for (final int[] element : resultat) { // CORRECTION : 'element' n'est
            // pas utilisé !!
            // somme = resultat[0][index] + resultat[1][index]; // CORRECTION :
            somme += element[index];
            // cette ligne n'a
            // pas besoin de la
            // boucle!!
            /* CORRECTION : sommme = element[index] */

        }

        Terminal.ecrireString("\nLe total pour cette intention est :   " + somme + " voix\n");

    }
}
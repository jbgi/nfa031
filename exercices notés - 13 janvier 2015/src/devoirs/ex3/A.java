package devoirs.ex3;

import utils.Terminal;

public class A {

    public static void main(final String[] args) {
        Terminal.ecrireStringln("Affiche les 100 plus petits nombres premiers:");
        for (int I = 1; I <= 100; I++) { /* CORRECTION: il ne faudrait
                                          * incrémenté 'I' que si I est premier. */
            /* La boucle for est initialisée à 1, s'arrête à 100 et augmente de
             * 1 en 1 */
            if (Premier(I)) {
                Terminal.ecrireString(I + " ");
            }
        }

    }

    /* CORRECTION: On pourrait simplifier la compréhension en nommant la méthode
     * 'nombreDeDiviseurs'. Par ailleurs, connaître le nombre de diviseur est
     * plus couteux que de simplement savoir si on nombre est premier: on peut
     * alors s'arreter dès que l'on a trouvé le premier diviseur */
    public static int UnEntier(final int n) {
        int Count = 2, I;

        /* Retourne la racine carrée */
        final double Racine = Math.sqrt(n);

        /* La boucle for est initialisée à 1, s'arrête à la racine carré du nbre
         * et augmente de 1 en 1 */
        for (I = 2; I <= Racine; I++) {
            if ((n % I) == 0) {
                Count++;
            }
        }
        /* si le reste de la division du nbre et I est O,continuer au nbre
         * supérieure,sinon s'arrêter */
        return Count;
    }

    public static boolean Premier(final int n) {
        return (UnEntier(n) == 2);
    }

}
